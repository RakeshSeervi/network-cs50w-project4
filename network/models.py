from django.contrib.auth.models import AbstractUser
from django.db import models


class User(AbstractUser):
    following = models.ManyToManyField('self', symmetrical=False, related_name="followers")

    def serializer(self):
        return {
            "id": self.id,
            "username": self.username,
            "following": [user.username for user in self.following.all()],
            "followers": [user.username for user in self.followers.all()],
        }


class Post(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE, related_name='posts')
    body = models.TextField(max_length=256)
    liked_by = models.ManyToManyField(User, related_name='liked_posts', blank=True)
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"<Post {self.id}>"

    def serialize(self):
        return {
            "id": self.id,
            "author": self.author.username,
            "body": self.body,
            "liked_by": [user.username for user in self.liked_by.all()],
            "timestamp": self.timestamp,
        }