
from django.urls import path

from . import views

urlpatterns = [
    path("", views.index, name="index"),
    path("login", views.login_view, name="login"),
    path("logout", views.logout_view, name="logout"),
    path("register", views.register, name="register"),
    path("new", views.new, name="new"),
    path("edit/<int:id>", views.edit, name="edit"),
    path('post/<int:id>', views.react, name="react"),
    path("network/<str:page>", views.page_content, name="page"),
    path("profile/<str:username>", views.profile, name="profile")
]
