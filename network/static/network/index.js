window.onpopstate = function(event){
    loadPage(event.state.page, event.state.pageNumber);
}

document.addEventListener('DOMContentLoaded', function (){

    alertDiv = document.getElementById('alert');
    postFormContainer = document.querySelector('#post-form-container');
    postFormContainer.style.display = 'none';
    loginPrompt = document.querySelector('#promptLogin');
    loginPrompt.style.display = 'none';
    profileContainer = document.querySelector('#profile');
    profileContainer.style.display = 'none';
    heading = document.createElement('h3');
    heading.id = 'contentHeading';
    posts = document.querySelector("#posts");

    let form = document.querySelector('#post-form');
    csrfToken = form.getElementsByTagName('input')[0].value;

    form.onsubmit = () => {
        let body = form.getElementsByClassName('post-form-body');
        fetch('/new', {
            method: 'POST',
            headers: {
                "X-CSRFToken": csrfToken
            },
            body: JSON.stringify({
                body: body[0].value,
            }
           )
        })
            .then(response => response.json())
            .then(data => {
                body[0].value = " ";
                history.pushState({page: 'home', pageNumber: 1}, ``, '#all/page1');
                loadPage('home',1, data);
            });
        return false;
    }

    document.querySelector('#all').addEventListener('click', () => {
        history.pushState({page: 'home', pageNumber: 1}, ``, `#all/page1`);
        loadPage('home');
    });
    if(document.querySelector('#followedPosts')){
        document.querySelector('#followedPosts').addEventListener('click', () => {
            history.pushState({page: 'following', pageNumber: 1}, ``, '#following/page1');
            loadPage('following');
        });
    }
    if(document.querySelector('#user')) {
        document.querySelector('#user').addEventListener('click', () => {
            let page = document.querySelector('#user').dataset.page;
            history.pushState({page: page, pageNumber: 1}, ``, `#${page}/page1`);
            loadPage(page);
        });
    }

    if(window.location.hash){
        loadPage(history.state.page, history.state.pageNumber);
    }
    else{
        history.pushState({page: 'home', pageNumber: 1}, ``, '#all/page1');
        loadPage('home');
    }
});

function loadPage(page, pageNumber=1, message=null){
    alertDiv.innerHTML = "";
    posts.innerHTML = ""

    if(message){
        if(message.message){
            Alert = getAlertElement(message.message, "alert-success");
        }
        else{
            Alert = getAlertElement(message.error, "alert-danger");
        }
        alertDiv.appendChild(Alert);
    }

    let postList = document.createElement('div');
    postList.style.minHeight = "525px";
    let footer = document.createElement('div');

    if(page==='home') {
        postList.style.minHeight = '325px';
        heading.innerHTML = "All posts";
    }
    else if(page==='following'){
        heading.innerHTML = "Posts you follow"
    }
    else{
        postList.style.minHeight = '450px';
        fetch(`/profile/${page}`)
            .then(response => response.json())
            .then(data => {
                heading.innerHTML = `${page}'s posts`;
                profileContainer.getElementsByTagName('h3')[0].innerHTML = page;
                profileContainer.querySelector('#following').innerHTML = data.profile.following.length;
                profileContainer.querySelector('#followers').innerHTML = data.profile.followers.length;
                profileContainer.querySelector('#followers').dataset.followers = data.profile.followers.length;
                
                profileContainer.querySelector('#button').innerHTML = '';
                
                if(data.user && data.user!==page){
                    let follow = document.createElement('button');
                    follow.dataset.follow = data.profile.followers.indexOf(data.user)>-1? '1': '0';
                    follow.className = data.profile.followers.indexOf(data.user)>-1? 'btn btn-outline-danger btn-sm': 'btn btn-outline-success btn-sm';
                    follow.innerHTML = follow.dataset.follow==='1'? 'Unfollow': 'Follow';
                    follow.onclick = () => {
                        fetch(`/profile/${page}`,{
                            method: 'PUT',
                            headers: {
                                "X-CSRFToken": csrfToken
                            },
                            body: JSON.stringify({
                                action: follow.dataset.follow
                            })
                        })
                            .then(() => {
                                let msg1 = `You are now following ${page}!`;
                                let msg2 = `You unfollowed ${page}!`;
                                let followerContainer = profileContainer.querySelector('#followers');
                                followerContainer.dataset.followers = follow.dataset.follow==='1'? `${parseInt(followerContainer.dataset.followers)-1}`: `${parseInt(followerContainer.dataset.followers)+1}`;
                                followerContainer.innerHTML = followerContainer.dataset.followers;
                                follow.dataset.follow = follow.dataset.follow==='1'? '0': '1';
                                follow.innerHTML = follow.dataset.follow==='1'? 'Unfollow': 'Follow';
                                follow.className = follow.dataset.follow==='1'? 'btn btn-outline-danger btn-sm': 'btn btn-outline-success btn-sm';
                                alertDiv.innerHTML = "";
                                follow.dataset.follow==="1"? alertDiv.appendChild(getAlertElement(msg1, 'alert-success')): alertDiv.appendChild(getAlertElement(msg2, 'alert-danger'));
                            });
                    }
                    profileContainer.querySelector('#button').innerHTML = "";
                    profileContainer.querySelector('#button').appendChild(follow);
                }
            })
    }

    fetch(`/network/${page}?page=${pageNumber}`)
        .then(response => response.json())
        .then(data => {
            if(page==='home'){
                profileContainer.style.display = 'none';
                if(data.loggedIn){
                    postFormContainer.style.display = "block";
                    loginPrompt.style.display = 'none';
                }
                else{
                    postFormContainer.style.display = "none";
                    loginPrompt.style.display = 'block';
                }
            }
            else if(page=='following'){
                postFormContainer.style.display = 'none';
                loginPrompt.style.display = 'none';
                profileContainer.style.display = 'none';
            }
            else{
                postFormContainer.style.display = 'none';
                loginPrompt.style.display = 'none';
                profileContainer.style.display = 'block';
            }

            for(let post of data.posts){
                postList.appendChild(getPostElement(post, data.user));
            }

            footer.appendChild(getFooter(page, data.page, pageNumber));
        });
    posts.appendChild(heading);
    posts.appendChild(postList);
    posts.appendChild(footer);
}

function getAlertElement(message, statusClass){

    let alertSpan = document.createElement('span');
    alertSpan["aria-hidden"] = "true";
    alertSpan.innerHTML = "&times";

    let alertButton = document.createElement('button');
    alertButton.type = 'button';
    alertButton.className = 'close';
    alertButton.setAttribute("data-dismiss", "alert");
    alertButton.setAttribute("aria-label", "Close");
    alertButton.appendChild(alertSpan);

    let alertMessage = document.createElement('span');
    alertMessage.innerHTML = message;

    let Alert = document.createElement('div');
    Alert.className = 'alert alert-dismissable fade show ';
    Alert.role = "alert";
    Alert.appendChild(alertButton);
    Alert.appendChild(alertMessage);

    Alert.className+=statusClass;

    return Alert
}

function getPostElement(post, user){
    let postDiv = document.createElement('div');
    postDiv.className = 'post';

    let header = document.createElement('div');
    header.className = 'post-header';

    let anchor = document.createElement('a');
    anchor.href = 'javascript:void(0);';
    anchor.onclick = () => {
        history.pushState({page: post.author, pageNumber: 1}, ``, `#${post.author}/page1`);
        loadPage(post.author);
    }
    let name = document.createElement('b');
    name.innerHTML = post.author;
    anchor.appendChild(name);
    header.appendChild(anchor);

    let time = document.createElement('small');
    time.innerHTML = ` posted ${moment(post.timestamp).fromNow()}`;
    header.appendChild(time);

    let body = document.createElement('div');
    body.className = 'post-body';

    let bodyContent = document.createElement('div');
    bodyContent.innerHTML = post.body;
    let bodyActions = document.createElement('div');
    bodyActions.className = 'bodyActions';
    body.appendChild(bodyContent);

    let footer = document.createElement('div');
    footer.className = 'post-footer';

    let heart = document.createElement('i');
    heart.dataset.liked = (post.liked_by.indexOf(user))>-1? '1': '0';
    heart.dataset.likeCount = post.liked_by.length;

    if(heart.dataset.liked==='1'){
        heart.className = 'fas fa-heart filled';
    }
    else{
        heart.className = 'far fa-heart';
        if(user){
            heart.className += ' empty';
        }
    }
    if(heart.dataset.likeCount!=='0'){
        heart.innerHTML = ` ${heart.dataset.likeCount}`;
    }

    heart.addEventListener('click', (e)=>{
        if(user){
            react(e, post);
        }
    });

    footer.appendChild(heart);

    if(post.author===user) {
        let editButton = document.createElement('button');
        editButton.className = 'btn btn-dark btn-sm';
        editButton.innerHTML = '<i class="fas fa-edit"></i>';
        let textArea = document.createElement('textarea');

        editButton.onclick = (e) => {
            textArea.name = "body";
            textArea.rows = 4;
            textArea.cols = 64;
            textArea.maxLength = 256;
            textArea.required = true;
            textArea.value = post.body;
            bodyContent.innerHTML = "";
            bodyContent.appendChild(textArea);

            let cancel = document.createElement('button');
            cancel.innerHTML = "<i class='fas fa-times'>";
            cancel.className = "btn btn-outline-danger btn-sm";
            cancel.onclick = () => {
                bodyContent.innerHTML = post.body;
                bodyActions.removeChild(cancel);
                bodyActions.removeChild(submit);
                bodyActions.appendChild(editButton);
            }

            let submit = document.createElement('button');
            submit.innerHTML = '<i class="fas fa-paper-plane">';
            submit.className = "btn btn-outline-success btn-sm";
            submit.onclick = () => {
                fetch(`/edit/${post.id}`, {
                    method: 'PUT',
                    headers: {
                        "X-CSRFToken": csrfToken
                    },
                    body: JSON.stringify({
                        body: textArea.value
                    })
                })
                    .then(response => response.json())
                    .then(data => {
                        bodyContent.innerHTML = data.body.toString();
                        bodyActions.removeChild(submit);
                        bodyActions.removeChild(cancel);
                        bodyActions.appendChild(editButton);
                    });
            }

            bodyActions.removeChild(editButton);
            bodyActions.appendChild(submit);
            bodyActions.appendChild(cancel);
        }
        bodyActions.appendChild(editButton);
        body.appendChild(bodyActions);
    }

    let hr = document.createElement('hr');

    postDiv.id = `post${post.id}`;
    postDiv.appendChild(header);
    postDiv.appendChild(body)
    postDiv.appendChild(footer)
    postDiv.appendChild(hr);

    return postDiv;
}

function react(e, post){
    let element = e.originalTarget;

    fetch(`/post/${post.id}`,{
            method: "PUT",
            headers: {
                "X-CSRFToken": csrfToken
            },
            body: JSON.stringify({
                liked: e.target.dataset.liked
            })

        })
            .then(() => {
                if(element.dataset.liked==='1'){
                    element.className = "far fa-heart empty";
                    element.dataset.likeCount = `${parseInt(element.dataset.likeCount)-1}`;
                    if(element.dataset.likeCount!=='0') {
                        element.innerHTML = ` ${element.dataset.likeCount}`;
                    }
                    else{
                        element.innerHTML = ""
                    }
                    element.dataset.liked = '0';
                }
                else{
                    element.className = "fas fa-heart filled";
                    element.dataset.likeCount = `${parseInt(element.dataset.likeCount)+1}`
                    element.innerHTML = ` ${parseInt(element.dataset.likeCount)}`;
                    element.dataset.liked = '1';
                }
            });
}

function getFooter(page, pageInfo, pageNumber){
    let nav = document.createElement('nav');
    nav.className = "pager";

    let left = document.createElement('button');
    left.className = 'previous btn btn-dark btn-sm';
    left.innerHTML = "< Newer posts";

    let center = document.createElement('div');
    center.className = "pageStatus"
    center.innerHTML = `Showing page ${pageNumber} of ${pageInfo.total}`;

    let right = document.createElement('button');
    right.className = 'next btn btn-dark btn-sm';
    right.innerHTML = "Older posts >";

    if(pageNumber===1){
        left.disabled = true;
    }
    if(pageNumber===pageInfo.total){
        right.disabled = true;
    }

    left.addEventListener('click', ()=>{
        history.pushState({page: page, pageNumber: pageNumber-1}, ``, `#${page}/page${pageNumber-1}`);
        loadPage(page, pageNumber=pageNumber-1);
    });
    right.addEventListener('click', ()=>{
        history.pushState({page: page, pageNumber: pageNumber+1}, ``, `#${page}/page${pageNumber+1}`);
        loadPage(page, pageNumber=pageNumber+1);
    });

    nav.appendChild(left);
    nav.appendChild(center);
    nav.appendChild(right);

    return nav;
}
