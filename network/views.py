from django.contrib.auth import authenticate, login, logout
from django.db import IntegrityError
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import render
from django.urls import reverse
from django.core.paginator import Paginator
import json

from .models import User, Post
from .forms import PostForm


def index(request):
    post_form = PostForm()
    post_form.fields['body'].widget.attrs['class'] = 'post-form-body'
    return render(request, "network/index.html", {
        "postForm": post_form
    })


def login_view(request):
    if request.method == "POST":

        # Attempt to sign user in
        username = request.POST["username"]
        password = request.POST["password"]
        user = authenticate(request, username=username, password=password)

        # Check if authentication successful
        if user is not None:
            login(request, user)
            return HttpResponseRedirect(reverse("index"))
        else:
            return render(request, "network/login.html", {
                "message": "Invalid username and/or password."
            })
    else:
        return render(request, "network/login.html")


def logout_view(request):
    logout(request)
    return HttpResponseRedirect(reverse("index"))


def register(request):
    if request.method == "POST":
        username = request.POST["username"]
        email = request.POST["email"]

        # Ensure password matches confirmation
        password = request.POST["password"]
        confirmation = request.POST["confirmation"]
        if password != confirmation:
            return render(request, "network/register.html", {
                "message": "Passwords must match."
            })

        # Attempt to create new user
        try:
            user = User.objects.create_user(username, email, password)
            user.save()
        except IntegrityError:
            return render(request, "network/register.html", {
                "message": "Username already taken."
            })
        login(request, user)
        return HttpResponseRedirect(reverse("index"))
    else:
        return render(request, "network/register.html")


def page_content(request, page):
    posts = Post.objects.all().order_by("-timestamp")

    if page == 'following':
        posts = posts.filter(author__in=request.user.following.all())
    elif page != 'home':
        user = User.objects.filter(username=page).first()
        posts = posts.filter(author=user)

    paginator = Paginator(posts, 10)
    page_posts = paginator.get_page(request.GET.get('page'))

    return JsonResponse({
        "user": request.user.username,
        "posts": [post.serialize() for post in page_posts],
        "loggedIn": request.user.is_authenticated,
        "page": {
            "total": paginator.num_pages,
            "hasNext": page_posts.has_next(),
            "hasPrevious": page_posts.has_previous()
        }
    })


def new(request):
    if request.user.is_authenticated and request.method == 'POST':
        data = json.loads(request.body)
        form = PostForm(data)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.save()
            return JsonResponse({
                'message': 'Your post is up now!'
            }, status=201)
        else:
            return JsonResponse({
                'error': "Post body cannot be empty!"
            })


def edit(request, id):
    post = Post.objects.filter(id=id).first()

    if post and request.user.is_authenticated and post.author == request.user and request.method == 'PUT':
        data = json.loads(request.body)
        form = PostForm(data, instance=post)
        if form.is_valid():
            post = form.save()

            return JsonResponse({
                "body": post.body
            }, status=200)


def react(request, id):
    post = Post.objects.filter(id=id).first()
    data = json.loads(request.body)
    if post and request.user.is_authenticated and request.method == 'PUT':
        if data.get('liked') == '1':
            post.liked_by.remove(request.user)
        else:
            post.liked_by.add(request.user)

        post.save()

        return HttpResponse(status=204)


def profile(request, username):
    user = User.objects.filter(username=username).first()

    if user:
        if request.method == 'PUT':
            data = json.loads(request.body)
            if data['action'] == '1':
                user.followers.remove(request.user)
            else:
                user.followers.add(request.user)
            user.save()

            return HttpResponse(status=204)

        return JsonResponse({
            "user": request.user.username,
            "profile": user.serializer()
        })
