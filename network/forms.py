from .models import Post
from django.forms import ModelForm, Textarea, Form


class PostForm(ModelForm):
    class Meta:
        model = Post
        fields = ['body']
        widgets = {
            'body': Textarea(attrs={'cols': 64, 'rows': 4})
        }

